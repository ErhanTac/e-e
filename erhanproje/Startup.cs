﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(erhanproje.Startup))]
namespace erhanproje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
