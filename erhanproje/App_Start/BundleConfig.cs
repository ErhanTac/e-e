﻿using System.Web;
using System.Web.Optimization;

namespace erhanproje
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/bundles/custom").Include(
                      "~/Scripts/smoothscroll.js",
                      "~/Scripts/jquery.debouncedresize.js",
                      "~/Scripts/retina.min.js",
                      "~/Scripts/jquery.placeholder.js",
                      "~/Scripts/jquery.hoverIntent.min.js",
                      "~/Scripts/twitter/jquery.tweet.min.js",
                      "~/Scripts/jquery.flexslider-min.js",
                      "~/Scripts/owl.carousel.min.js",
                       "~/Scripts/jflickrfeed.min.js",
                       "~/Scripts/jquery.prettyPhoto.js",
                       "~/Scripts/main.js",
                       "~/Scripts/jquery.themepunch.tools.js",
                       "~/Scripts/jquery.themepunch.revolution.js"));

        }
    }
}
